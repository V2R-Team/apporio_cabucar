<?php
function AndroidPushNotificationCustomer($did, $msg,$ride_id,$ride_status) 
{
	// Set POST variables
	$url = 'https://fcm.googleapis.com/fcm/send';
	
	$app_id="1";
	
	$fields = array ('to' => $did,'data' => array('message' => $msg,'ride_id'=>$ride_id,'ride_status'=> $ride_status,'app_id'=>$app_id) );

	$headers = array (
			'Authorization:    key=AAAAw0pWfR0:APA91bE58ocGah6tDoekgIKzaKAxb9ITgR2bk-zBBjLb4O2MCwUqC9tuWyiieTYGjAnOOa9z5CQFB67reH_34bX4QtW8BGgnTTEYCzdh2xgvJFi7t95fURG0x0fP5QH1jl5ai_ITVEjq',
			'Content-Type: application/json' );

	// Open connection
	$ch = curl_init ();
	// Set the url, number of POST vars, POST data
	curl_setopt ( $ch, CURLOPT_URL, $url );
	curl_setopt ( $ch, CURLOPT_POST, true );
	curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
	curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 ); 
	curl_setopt ( $ch, CURLOPT_POSTFIELDS,  json_encode ( $fields ) );
	// Execute post
 	$result = curl_exec ( $ch );
	// Close connection
	curl_close ( $ch );
		return $result;
}

function AndroidPushNotificationDriver($did, $msg,$ride_id,$ride_status) {
	// Set POST variables
	$url = 'https://fcm.googleapis.com/fcm/send';
	
	$app_id="2";
	
	
	

	$fields = array ('to' => $did,'data' => array('message' => $msg,'ride_id'=>$ride_id,'ride_status'=> $ride_status,'app_id'=>$app_id) );

	$headers = array (
			'Authorization: key=AAAAw0pWfR0:APA91bE58ocGah6tDoekgIKzaKAxb9ITgR2bk-zBBjLb4O2MCwUqC9tuWyiieTYGjAnOOa9z5CQFB67reH_34bX4QtW8BGgnTTEYCzdh2xgvJFi7t95fURG0x0fP5QH1jl5ai_ITVEjq',
			'Content-Type: application/json' );
	// Open connection
	$ch = curl_init ();
	// Set the url, number of POST vars, POST data
	curl_setopt ( $ch, CURLOPT_URL, $url );
	curl_setopt ( $ch, CURLOPT_POST, true );
	curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
	curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 ); 
	curl_setopt ( $ch, CURLOPT_POSTFIELDS, json_encode ( $fields ) );
	// Execute post
	$result = curl_exec ( $ch );
	// Close connection
	curl_close ( $ch );
        return $result;
}
?>