<?php
error_reporting(0);
include_once '../apporioconfig/start_up.php';

$user_id=$_REQUEST['user_id'];
$order_id=$_REQUEST['order_id'];
$payment_method=$_REQUEST['payment_method'];
$payment_amount=$_REQUEST['payment_amount'];
$payment_date_time=$_REQUEST['payment_date_time'];

  $query="select * from user where user_id = $user_id ";
  $result = $db->query($query);
  $list=$result->rows;
  foreach($list as $login);

  $query1="select * from ride_table where ride_id = $order_id ";
  $result1 = $db->query($query1);
  $list1=$result1->rows;
  foreach($list1 as $login1);

  $query122="select * from done_ride where ride_id = $order_id ";
  $result122 = $db->query($query122);
  $list122=$result122->rows;
  foreach($list122 as $login122);

  $query2="select * from car_type where car_type_id = '".$login1['car_type_id']."' ";
  $result2 = $db->query($query2);
  $list2=$result2->rows;
  foreach($list2 as $login2);

  $rate="select * from rate_card where car_type_id = '".$login2['car_type_id']."' ";
  $rates = $db->query($rate);
  $ratess = $rates->row;  

  $query3="select * from car_model where car_type_id = '".$login2['car_type_id']."' ";
  $result3 = $db->query($query3);
  $list3=$result3->rows;
  foreach($list3 as $login3);

?>




<html>
  <head>
    <meta charset="utf-8">
    <title> Mail  </title>

    <style media="screen">
      @media (max-width: 461px){
        .maindiv{
          width: 100% !important;
        }
      }

      @media only screen and (min-width : 461px) and (max-width : 767px) {
        .maindiv{
          width: 100% !important;
        }
      }

      @media only screen and (min-width : 768px) and (max-width :992px) {
        .maindiv{
          width: 100% !important;
        }
      }

    </style>
  </head>
  <body style="background:#eee;">
    <div class="maindiv" style="width:70%; margin:0px auto; border:1px solid #ccc; background:#fff; padding:15px; font-family:arial;">
      <table width="100%">
        <tr>
          <td style="text-align:left;"> <?php echo $payment_date_time; ?> </td>
          <td style="text-align:right;"> Taxi App  </td>
        </tr>

        <tr>
          <td colspan="2" style="text-align:center; padding:20px 0px 0px 0px; font-size:46px; font-weight: bold;"> Rs. <?php echo round($payment_amount); ?>  </td>
        </tr>

        <tr>
          <td colspan="2" style="text-align:center; color:#707070; padding:20px 0px 0px 0px; font-size:14px;"> CRN530048250  </td>
        </tr>

        <tr>
          <td colspan="2" style="text-align:center; color:#000; border-bottom:3px solid #eee;  padding:20px 0px 5px 0px; font-size:14px;"> Thanks for travelling with us, <?php echo $login['user_name'] ?>  </td>
        </tr>
      </table>


      <div>
        <table width="48%" class="maindiv"  style="float:left;">
          <tr>
            <th style="text-align:center; padding:20px 0px;"> Ride Details  </th>
          </tr>

          <tr>
            <td>
              <table cellspacing="0" width="100%" style="color:#707070; font-size:14px;">
                <tr style="border:2px solid #eee !important;">
                  <td style="text-align:left; padding:8px; border-bottom: 2px solid #eee;" width="50"> <img src="../<?php echo $login['user_image'] ?> " alt="" width='50'/> </td>
                  <td style="text-align:left; padding:8px; color:#000; border-bottom: 2px solid #eee; font-size:18px;"> <?php echo $login['user_name'] ?> </td>
                </tr>

                <tr style="border:2px solid #eee !important;">
                  <td style="text-align:left; padding:8px; border-bottom: 2px solid #eee;" width="50"> <img src="mail/minter.png" alt="" width='50'/> </td>

                  <?php $timess = explode(':',$login122['tot_time']);?>

                  <td style="text-align:left; padding:8px; color:#000; border-bottom: 2px solid #eee; font-size:15px;"> <?php echo $login122['distance']; ?> miles <br>   <?php echo $timess[0];?> hour <br> <?php echo $timess[1];?> minutes <br><?php echo $timess[2];?> seconds </td>
                </tr>


                <tr style="border:2px solid #eee !important;">
                  <td style="text-align:left; padding:8px; border-bottom: 2px solid #eee;" width="50"> <img src="mail/car.png" alt="" width='50'/> </td>
                  <td style="text-align:left; padding:8px; color:#000; border-bottom: 2px solid #eee; font-size:15px;"> <?php echo $login2['car_type_name'] ?> - <?php echo $login3['car_model_name'] ?></td>
                </tr>

                <tr>
                  <td colspan="2">
                    <div style="padding:10px;">
                      <div style="float:left;">
                        <div style="min-height:75px;">
                          <?php echo date('h:i A', strtotime($login122['begin_time'])); ?>
                        </div>

                        <div style="">
                          <?php echo date('h:i A', strtotime($login122['end_time'])); ?>
                        </div>
                      </div>

                      <div style="float:left; margin:10px 20px;">
                        <img src="mail/border.png" alt="" />
                      </div>

                      <div style="float:right; width: 55%;">
                        <div style="">
                          <?php echo $login122['begin_location'] ?>
                        </div>

                        <br>

                        <div style="">
                          <?php echo $login122['end_location'] ?>
                        </div>
                      </div>
                      <div style="clear:both;"></div>
                    </div>
                  </td>
                </tr>


              </table>
            </td>
          </tr>
        </table>

        <table width="48%" class="maindiv"  style="float:right;">
          <tr>
            <th style="text-align:center; padding:20px 0px;"> Bill Details  </th>
          </tr>

          <tr>
            <td>
              <table cellspacing="0" width="100%" style="border-top:2px solid #eee; color:#707070; font-size:14px;">
                <tr>
                  <td style="text-align:left; padding:8px;"> Base Fare </td>
                  <td style="text-align:right; padding:8px;"> <?php echo $ratess['first_2km']; ?> </td>
                </tr>
                <tr>
                  <td style="text-align:left; padding:8px;"> Distance Fare for <?php echo $login122['distance']; ?> miles </td>


                  <?php
                      $dist = $login122['distance'];
                    if($dist<2){
                        $total = 0;                      
                    }else{ 
                        $after = $ratess['after_2km']; 
                        $dist = $login122['distance'];
                        $total = $basfare + $after *($dist-2);
                    }

                  ?>


                  <td style="text-align:right; padding:8px;"> Rs.<?php echo $total; ?></td>
                </tr>
                <tr style="background:#f3f3f3;">
                  <td style="text-align:left; padding:8px;">Total Fare </td>

                    <?php
                        $a = $ratess['first_2km']; 
                        $b = $total;
                        $c = $a+$b;
                    ?>

                  <td style="text-align:right; padding:8px;"> Rs. <?php echo $c; ?> </td>
                </tr>
                <tr>
                  <td style="text-align:left; padding:8px;"> Payment Method </td>
                  <td style="text-align:right; padding:8px;"> Rs. <?php echo $payment_method; ?> </td>
                </tr>
                <tr style="background:#f3f3f3;">
                  <th style="text-align:left; padding:10px; color:#000; font-size:16px;">Total Bill <span style="color:#707070; font-size:14px; font-weight: normal;">(rounded off)</span> </th>
                  <th style="text-align:right; padding:10px; color:#000; font-size:17px;"> Rs. <?php echo round($payment_amount); ?> </th>
                </tr>

                <tr>
                  <td colspan="2" style="text-align:left; padding:8px;">
                    To download invoice or raise queries, <br>
                    <a href="#" style="color:#5d93bb; text-decoration: none; line-height: 25px;">find support for this ride.</a>
                  </td>
                </tr>
              </table>
            </td>
          </tr>

        </table>
        <div style="clear:both;"></div>
      </div>

      <div style="margin:20px 0px;">
        <table width="100%">
          <tr>
            <th colspan="2" style="text-align:center; padding:8px;"> Payment </th>
          </tr>
          <tr>
            <td style="padding:15px; color:#707070; border-top:1px solid #ddd; border-bottom:1px solid #ddd;"> Paid by Taxi App Money Wallet </td>
            <td style="padding:15px; color:#707070; border-top:1px solid #ddd; border-bottom:1px solid #ddd;"> Rs. <?php echo round($payment_amount); ?>  </td>
          </tr>
        </table>
      </div>

    </div>
  </body>
</html>




    